
---

# Jenkins and the DevOps lifecycle

<img src="../99_misc/.img/devops.png" alt="devops" style="float:right;width:400px;">

The DevOps Life Cycle consists of eight stages in the planning, development, and operation of a system, or software application. Jenkins fits into the DevOps Life Cycle and how using Jenkins ties into continuous integration and continuous deployment. 

The DevOps Life Cycle is presented as an infinity symbol because the cycle is continuous. Each step is repeated in order until the system or application is decommissioned. The loop is divided into two groups: development and operational
In the development group:
- plan
- code
- build
- test

In the operations group we:
- cycle
- deploy
- operate
- monitor

Jenkins is the perfect tool for automating processes, tied to the build, test, release and deploy stages. When tools like Jenkins are used to automate the build and test stages, the process is known as continuous integration. Using automation in the release and deploy stages is called continuous delivery. And if the process is completely automated, it can be referred to as continuous deployment. The main goal of continuous integration is to find and resolve problems early in the development cycle. These steps also produce an artifact that can be deployed. 

---

# Create a pipeline project

Let's create our first pipeline job. By clicking new item, and then giving it a name we'll be able to initiate the project. Let's call it first pipeline. And then I'll select pipeline for the project type. 
We'll select `hello world`. And that will enter a pipeline script that prints the message "hello world" in one stage called hello. So I will save that and then click build now. And I'll give it a second for this job to complete. Okay, the job is done, and I'm just going to pause here. I want you to take a moment to explore the interface a bit. And if you've used Jenkins before, it should be pretty familiar, perhaps with a few new additions. This pipeline was fairly simple. So before we turn up the complexity, let's discuss all the parts that make up a pipeline. 
 (New-Item --> Pipeline -|| Name (first-pipeline) || 'OK' --> click 'Pipeline' --> choose 'hello-world' project --> press 'Save')


---

# Practice

- Create pipeline:
    - Hello stage, that prints 'hello world' and sleeps 2 seconds
    - Goodbye stage, that prints 'Goodbye world' and sleeps 2 seconds
- Run the pipeline

```groovy

pipeline{
    agent any

    stages{
        stage('Hello'){
            steps{
                echo 'Hello world'
                sleep 2
            }
        }
        stage('Goodbye'){
            steps{
                echo 'Goodbye world'
                sleep 2
            }
        }
    }
}
```


---

# Declarative Pipelines


| Scripted pipeline | Declarative pipeline |
| --- | --- |
| node {} | pipeline {}
| Groovy-based DSL | Specifically designed for configuring jenkins projects | 

example:

```groovy

pipeline{
    agent any

    stages{
        stage("Hello"){
            steps{
                echo 'Hello world'
            }
        }
    }
}
```

---

# Declarative Pipelines

`Agent` specifies where pipelines will run:

|   Command |  Explanation |
|   ---     |  ---          |
| agent any |  run on first available system |
| agent { label 'linux' } | run on a system with specific label |
| agent { docker { image 'python:3.7' }} | run the pipeline inside  a docker container using specific name |
| agent none |  defer section to stages | 

---

# Practice

- Create new pipeline named minimal_pipeline
- Copy the code below to the pipeline we just created
- Run the pipeline

```groovy
pipeline{
    agent any

    stages {
        stage('Getting Requirements'){
            steps{
                echo 'Checking and Installing Requirements ...'
                sleep 1
            }
        }
        stage('Building ...'){
            steps{
                echo 'Building binary files ...'
                sleep 1
            }
        }
        stage('Test'){
            steps{
                echo 'Test 1 ...'
                sleep 1
                echo 'Test 2 ...'
                sleep 1
                echo 'Test 3 ...'
                sleep 1
            }
        }
        stage('Report'){
            steps{
                echo 'Reporting'
                sleep 1
            }
        }
    }
}

```

---

# Pipeline steps and snippets

While using jenkins pipelines, printing and sleeping is not enough, so here are additional command that we'll practice for other pipelines

| command  |  explanation | 
| --- | --- |
| echo | print message |
| git  | checkout code from git repo |
| sh   | run shell commands or script |
| archiveArtifact | archive artifacts |

You are welcome to see the compete list of commands [Jenkins Documentation](https://jenkins.io/doc/pipeline/steps/workflow-basic-steps)

---

# Pipeline steps and snippets (cont.)

With all steps, it can be difficult to know how to write the code that calls a specific step along with any parameters or arguments that we need to pass to the step. Fortunately for us, Jenkins provides a pipeline syntax generator that we can use to create snippets of code that we can copy into a pipeline.
Let have an example and copy the code to to new pipeline.

```groovy
pipeline{
    agent any

    stages {
        stage('Report'){
            steps{
                sh 'echo "this is a report" > report.txt'
            }
        }

```

---
# Pipeline steps and snippets (cont.)


Now lets click ![](../99_misc/.img/pipeline_snippet.png) link for us to see the snippet page.
As you can see there are several types of snippet generators, yet for now, let's focus on the snippet generator. If we click the selector under sample step, we can see that the basic steps are provided here for us to choose from and I'll just stick with the `archive artifacts`, which is the first step in the list. In this dialogue, we would enter all the parameters that we will want to pass to the step. 

For example, if we wanted to archive all of the `TXT` files that were generated by a step in my pipeline, then we'll use `*.TXT` in the files to archive field. 
Many command steps have different and advanced configuration that is somewhat self-explanatory, so feel free to explore. Now that we have the form filled out, all we need to do is click generate pipeline script. This creates a snippet that we can use in my pipeline. So we'll select all of the text here and copy it back in our code.

```groovy
pipeline{
    agent any

    stages {
        stage('Report'){
            steps{
                sh 'echo "this is a report" > report.txt'
                archiveArtifacts allowEmptyArchive: true, artifacts: *.txt
            }
        }
    }
```

---

# Practice

- Create pipeline named pc_data_report
- Write a python script to generate report about your device and save it as csv file
- Create a pipeline that will:
    - Add permissions and Run python script 
    - Archive the data.csv

Python script

```python

#!/usr/bin/env python3

import platform

with open('data.csv','w') as data_file:
    data_file.write(platform.machine())
    data_file.write(',')
    data_file.write(platform.platform())
    data_file.write(',')
    data_file.write(platform.node())
    data_file.write(',')
    data_file.write(platform.python_version())

```
Pipeline code

```groovy

pipeline{
    agent any

    stages {
        stage('Generating script')
                echo 'Creating script'
                sh """
                    echo '
#!/usr/bin/env python3

import platform

with open('data.csv','w') as data_file:
    data_file.write(platform.machine())
    data_file.write(',')
    data_file.write(platform.platform())
    data_file.write(',')
    data_file.write(platform.node())
    data_file.write(',')
    data_file.write(platform.python_version())
                    ' > data_script.py
                """
                sleep 2
            }
        stage('Running script')
                sh """
                    chmod +x data_script.py
                    python3 data_script.py
                """
            }
        stage('Archiving'){
                archiveArtifacts allowEmptyArchive: true, artifacts: data.csv
            }
        }

```

---