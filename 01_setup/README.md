
---

# Jenkins Setup

---
Our course is practical, meaning that everything we'll talk about or mention, will be practiced in one manner or another.
```sh
git clone https://gitlab.com/silent-mobius/jenkins-shallow-dive.git

cd jenkins-shallow-dive/

git pull origin master

cd 99_misc/setup/docker
```
---

# Jenkins Setup (cont.)

Due to technological advances we have choice of either running it in containerized environment such as Docker or, installing and running Jenkins directly on our host.
For Docker run th commands below:

```sh
./setup.sh # this will install docker and docker-compose
docker-compose up -d # this should start the docker containers

```

This will run dockerize Jenkins master and slave
Find your instance public ip address : 
Open in browser - e.g : http://1.2.3.4


In case of docker environment is not required or we just want to run Jenkins on bare-metal/virtual-machine

```sh
cd 99_misc/setup/vm
./setup.sh # wait for the script to finish
vagrant up
```

---

# Jenkins Setup (cont.)

## First login with __initialAdminPassword__

<img src="../99_misc/.img/jenkins_login.png" alt="jenkins login" style="float:right;width:400px;">

Fortunately for us, installing is not enough and we need to log to our jenkins instance via browser, which will request for __initialAdminPassword__. The file is usually located at `/var/lib/jenkins/secrets` folder. In case you are using docker-compose version, __initialAdminPassword__ is printed in logs so we can use next command to get the __initialAdminPassword__.

```sh
docker-compose logs # yes, there are other ways to get the data,
# this one is the easiest
```

The output should look like this

<img src="../99_misc/.img/docker_jenkins_login.png" alt="docker logs" style="float:right;width:400px;">

---

# Jenkins Setup (cont.)

## Plugin install



---

# Jenkins Setup (cont.)

## Create user

- Create first admin user
        - Remember the password!!!


---

# Practice

- Please to install system
- Create additional user named "Tiny-Terry"
        - Manage users > Create user 
- 