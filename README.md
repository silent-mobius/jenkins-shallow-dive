
# Jenkins Deep Dive


##  About The Course Itself 
We'll learn several topics mainly focused on:

What is CI/CD ?
What is Jenkins ?
Who needs Jenkins ?
How Jenkins works ?
How to manage Jenkins in various environments?


## Who Is This course for ?

Junior/senior sysadmins who have no knowledge of CI/CD with Jenkins.
For junior/senior developers who to implement CI/CD on their private or corporate projects.
For DevOps engineers who wish to implement CI/CD with Jenkins with different groups they work with.
Experienced ops who need refresher in regards CI/CD or Jenkins.


- Intro
- About CI/CD
-  Jenkins Setup
-  Jenkins Fundamentals
    -  Builds
    -  Agents and Distributed Builds
    -  Extending Jenkins
    -  Notifications
    -  Testing
    -  RestAPI
    -  Artifacts
-  Jenkins Pipelines
    - node
    - stage, step and sh
    - built-in docs
    - scripted vs. declarative
    - executors, slaves and containers


## What course tries to accomplish ?

Eventual goal of the course is to teach its students all in regards to CI/CD <!--specified material --> and to elevate their knowledge to higher level, eventually making them professionals in the field.


---

> `[!]` Note: Please use `build.sh` script to create html version for your use

---
© All Right reserved to Alex M. Schapelle of VaioLabs ltd.